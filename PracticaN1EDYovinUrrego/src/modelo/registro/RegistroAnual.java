/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.registro;

/**
 *
 * @author Yovin
 */
public class RegistroAnual {
    private short year;
    private double[][] registro = new double[12][5];  

    /**
     * @return the year
     */
    public short getYear() {
        return year;
    }

    public RegistroAnual(short year) {
        this.year = year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(short year) {
        this.year = year;
    }

    /**
     * @return the registro
     */
    public double[][] getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(int mes, int agencia, double dimero) {
        this.getRegistro()[mes][agencia] = dimero;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(double[][] registro) {
        this.registro = registro;
    }

    
}
