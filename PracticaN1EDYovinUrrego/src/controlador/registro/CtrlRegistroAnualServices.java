/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.registro;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.table.DefaultTableModel;
import modelo.registro.RegistroAnual;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Yovin
 */
public class CtrlRegistroAnualServices {

    private CtrlRegistroAnual controlador = new CtrlRegistroAnual((short) 2021);
// Retorna la agencia con mayores ventas

    public int obtenerMayor() {
        return getControlador().obtenerMayor();
    }
// Retorna el mes que obtuvo menos ventas

    public int obtenerMenorMes() {
        return getControlador().obtenerMenorMes();
    }
// Retorna un array de las ventas de una determinada agencia

    public double[] ventasAgencia(int numero) {
        return getControlador().ventasAgencia(numero);
    }
// Agregar ventas

    public void agregarVentasA(int mes, int agencia, double dimero) {
        getControlador().agregarVentasA(mes, agencia, dimero);
    }
// retorna el promedio de ventas de un mes

    public double promedioMes(int mes) {
        return Math.round(controlador.promedioMes(mes) * 100.0) / 100.0;
    }
// Retorna el total de ventas de una agencia

    public long ventasTotalesAgencia(int agencia) {
        return getControlador().ventasTotalesAgencia(agencia);
    }

    public double promedioAgencia(int agencia) {
        return Math.round(controlador.promedioAgencia(agencia) * 100.0) / 100.0;
    }

    public int obtenerMayorMes() {

        return controlador.obtenerMayorMes();
    }

//Constructor
    public CtrlRegistroAnualServices() {
    }
//Genera datos aleatorios para probar el software

    public void generarMatriz() {
        getControlador().generarMatriz();
    }

    /**
     * @return the controlador
     */
    public CtrlRegistroAnual getControlador() {
        return controlador;
    }

    /**
     * @param controlador the controlador to set
     */
    public void setControlador(CtrlRegistroAnual controlador) {
        this.controlador = controlador;
    }

    public DefaultTableModel getTableRegistro() {

        return controlador.getTableRegistro();
    }

    public DefaultTableModel getTableRegistro(int agencia) {
        return controlador.getTableRegistro(agencia);
    }

    public String retornarMes(int mes) {
        return controlador.retornarMes(mes);
    }
    
    public void guardar() throws FileNotFoundException, IOException{
 // FileOutputStream f = new FileOutputStream("datos/"+RegistroAnual.class.getSimpleName()+".json");

controlador.guardar();
}
}
