/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.registro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import modelo.registro.RegistroAnual;
import org.json.simple.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yovin
 */
public class CtrlRegistroAnual extends RegistroAnual {

    public CtrlRegistroAnual(short year) {
        super(year);
    }

    public int obtenerMayor() {
        double ventasAgencia[] = new double[5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 12; j++) {
                ventasAgencia[i] = ventasAgencia[i] + super.getRegistro()[j][i];
            }
        }
        double mayor = ventasAgencia[0];
        int agencia = 0;
        for (int i = 0; i < 5; i++) {
            if (ventasAgencia[i] > mayor) {
                mayor = ventasAgencia[i];
                agencia = i;
            }
        }
        return agencia;
    }

    public int obtenerMayorMes() {
        double ventasMes[] = new double[12];
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 5; j++) {
                ventasMes[i] = ventasMes[i] + super.getRegistro()[i][j];
            }
        }
        double mayor = ventasMes[0];
        int mes = 0;
        for (int i = 0; i < 12; i++) {
            if (ventasMes[i] > mayor) {
                mayor = ventasMes[i];
                mes = i;
            }
        }
        return mes;
    }

    public int obtenerMenorMes() {
        double ventasMes[] = new double[12];
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 5; j++) {
                ventasMes[i] = super.getRegistro()[i][j] + ventasMes[i];
            }
        }
        double menor = ventasMes[0];
        int mes = 0;
        for (int i = 0; i < 12; i++) {
            if (ventasMes[i] < menor) {
                menor = ventasMes[i];
                mes = i;
            }
        }
        return mes;
    }
// Retorna un array de las ventas de una determinada agencia

    public double[] ventasAgencia(int numero) {
        double[] ventas = new double[12];
        for (int i = 0; i < 12; i++) {
            ventas[i] = super.getRegistro()[i][numero];
        }
        return ventas;
    }
// Agregar ventas

    public void agregarVentasA(int mes, int agencia, double dimero) {
        double venta = super.getRegistro()[mes][agencia] + dimero;
        super.setRegistro(mes, agencia, venta);
    }
// retorna el promedio de ventas de un mes

    public double promedioMes(int mes) {
        double promedio = 0;
        for (int i = 0; i < 5; i++) {
            promedio = promedio + super.getRegistro()[mes][i];
        }
        return promedio / 5;
    }

    // retorna el promedio de ventas de un mes
    public double promedioAgencia(int agencia) {
        double promedio = 0;
        for (int i = 0; i < 12; i++) {
            promedio = promedio + super.getRegistro()[i][agencia];
        }
        return promedio / 12;
    }
// Retorna el total de ventas de una agencia

    public long ventasTotalesAgencia(int agencia) {
        long total = 0;
        for (int i = 0; i < 12; i++) {
            total = total + (long) super.getRegistro()[i][agencia];
        }
        return total;
    }

    public void generarMatriz() {

        double matriz[][] = super.getRegistro();
        for (int i = 0; i < super.getRegistro().length; i++) {
            for (int j = 0; j < super.getRegistro()[i].length; j++) {
                matriz[i][j] = (int) (Math.random() * (1000000 + 1000000)) + 0;
            }
        }
        super.setRegistro(matriz);
    }

    public DefaultTableModel getTableRegistro() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Meses");
        modelo.addColumn("Agencia 1");
        modelo.addColumn("Agencia 2");
        modelo.addColumn("Agencia 3");
        modelo.addColumn("Agencia 4");
        modelo.addColumn("Agencia 5");
        String datos[] = new String[this.getRegistro()[0].length + 1];
        for (int i = 0; i < this.getRegistro().length; i++) {
            datos[0] = retornarMes(i);
            for (int j = 0; j < this.getRegistro()[i].length; j++) {
                datos[j + 1] = String.valueOf(this.getRegistro()[i][j]);
            }
            modelo.addRow(datos);
        }
        return modelo;
    }

    public DefaultTableModel getTableRegistro(int agencia) {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Meses");
        modelo.addColumn("Agencia " + agencia);
        String datos[] = new String[this.getRegistro()[0].length + 1];
        for (int i = 0; i < 12; i++) {
            datos[0] = retornarMes(i);
            datos[1] = String.valueOf(ventasAgencia(agencia)[i]);
            modelo.addRow(datos);
        }
        return modelo;
    }

    public String retornarMes(int mes) {
        String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        return meses[mes];
    }

    public void guardar() throws FileNotFoundException, IOException {
        FileWriter file = new FileWriter("datos/2021" + RegistroAnual.class.getSimpleName() + ".json");
        // FileOutputStream f = new FileOutputStream("datos/"+RegistroAnual.class.getSimpleName()+".json");
        for (int j = 0; j < 12; j++) {
            JSONObject archivoJson = new JSONObject();
            archivoJson.put(" Mes", retornarMes(j));
            for (int i = 0; i < 5; i++) {
                archivoJson.put("Agencia " + (i + 1), ventasAgencia(i)[j]);
            }
            file.write(archivoJson.toJSONString() + "\r\n");
            file.flush();
        }
    }
}
